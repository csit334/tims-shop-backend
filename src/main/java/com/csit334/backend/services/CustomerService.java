package com.csit334.backend.services;


import com.csit334.backend.entity.Admin;
import com.csit334.backend.entity.Customer;
import com.csit334.backend.repositories.CustomerRepository;
import com.csit334.backend.transfers.AddressTransfer;
import com.csit334.backend.transfers.AdminTransfer;
import com.csit334.backend.transfers.CustomerCreateTransfer;
import com.csit334.backend.transfers.CustomerTransfer;
import org.dynabiz.mapper.ModelMapper;
import org.dynabiz.std.exception.RepositoryException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Service
public class CustomerService {
    @Autowired
    private CustomerRepository customerRepository;



    public List<CustomerTransfer> findAll(){
        return ModelMapper.mapFromCollection(CustomerTransfer.class, customerRepository.findAll());
    }

    public CustomerTransfer findOne(long id){
        Customer customer = customerRepository.findById(id).orElseThrow(()->RepositoryException.ITEM_NOT_FOUND);
        CustomerTransfer transfer = ModelMapper.mapFrom(CustomerTransfer.class, customer);

        transfer.setAddress(ModelMapper.mapFrom(AddressTransfer.class, customer.getAddress()));
        return transfer;
    }


    public void update(CustomerTransfer transfer){
        Customer admin = customerRepository.findById(transfer.getId())
                .orElseThrow(()-> RepositoryException.ITEM_NOT_FOUND);
        ModelMapper.map(admin, transfer);
        ModelMapper.map(admin.getAddress(), transfer.getAddress());
        customerRepository.save(admin);
    }


    public void create(CustomerCreateTransfer transfer){

    }


    public void delete(long id){
        customerRepository.deleteById(id);
    }


}
