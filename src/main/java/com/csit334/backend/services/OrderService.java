package com.csit334.backend.services;

import com.csit334.backend.entity.*;
import com.csit334.backend.repositories.ModelItemRepository;
import com.csit334.backend.repositories.OrderRepository;
import com.csit334.backend.repositories.OrderItemRepository;
import com.csit334.backend.transfers.*;
import org.dynabiz.mapper.ModelMapper;
import org.dynabiz.std.exception.RepositoryException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderService {
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private OrderItemRepository orderItemRepository;
    @Autowired
    private ModelItemRepository modelItemRepository;

    @Transactional
    public OrderTransfer findOne(long id){
        CustomerOrder order = orderRepository.findById(id)
                .orElseThrow(()->RepositoryException.ITEM_NOT_FOUND);
        OrderTransfer transfer = ModelMapper.mapFrom(OrderTransfer.class, order);
        transfer.setItems(ModelMapper.mapFromCollection(OrderItemTransfer.class, orderItemRepository.findAllByOrderId(order.getId())));
        return transfer;
    }

    @Transactional
    public List<OrderTransfer> findAll(){
        return ModelMapper.mapFromCollection(OrderTransfer.class, orderRepository.findAll());
    }

    @Transactional
    public void update(OrderTransfer transfer){
        CustomerOrder order = orderRepository.findById(transfer.getId())
                .orElseThrow(()->RepositoryException.ITEM_NOT_FOUND);
        ModelMapper.map(order, transfer);

        orderRepository.save(order);
    }

    @Transactional
    public void create(OrderCreateTransfer transfer){
        CustomerOrder order = orderRepository.save(ModelMapper.mapFrom(CustomerOrder.class, transfer));
        List<OrderItem> items = ModelMapper.mapFromCollection(OrderItem.class, transfer.getItems(), (s,t)-> t.setOrderId(order.getId()));
        orderItemRepository.saveAll(items);

        // update model item stock
        List<ModelItem> modelItems = modelItemRepository.findAllById(items.stream().map(OrderItem::getModelItemId).collect(Collectors.toList()));
        Iterator<ModelItem> modelItemIterator = modelItems.iterator();
        for(OrderItem i : items){
            ModelItem modelItem = modelItemIterator.next();
            modelItem.setStock(modelItem.getStock() - i.getQuantity());
        }
        modelItemRepository.saveAll(modelItems);
    }

    @Transactional
    public void delete(long id){
        CustomerOrder order = orderRepository.findById(id)
                .orElseThrow(()->RepositoryException.ITEM_NOT_FOUND);
        orderItemRepository.deleteAll(orderItemRepository.findAllByOrderId(order.getId()));
        orderRepository.deleteById(order.getId());
    }


}
