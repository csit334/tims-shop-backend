package com.csit334.backend.services;


import com.csit334.backend.entity.Address;
import com.csit334.backend.entity.Admin;
import com.csit334.backend.repositories.AddressRepository;
import com.csit334.backend.repositories.AdminRepository;
import com.csit334.backend.transfers.*;
import org.dynabiz.mapper.ModelMapper;
import org.dynabiz.std.exception.RepositoryException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AdminService {
    @Autowired
    private AdminRepository adminRepository;
    @Autowired
    private AddressRepository addressRepository;


    public AdminTransfer findOne(long id){
        Admin admin =  adminRepository.findById(id).orElseThrow(()-> RepositoryException.ITEM_NOT_FOUND);

        Address address = addressRepository.findById(admin.getAddressId())
                .orElseThrow(()-> RepositoryException.ITEM_NOT_FOUND);
        AdminTransfer transfer = ModelMapper.mapFrom(AdminTransfer.class, admin);
        transfer.setAddress(ModelMapper.mapFrom(AddressTransfer.class, address));
        return transfer;
    }

    public List<AdminTransfer> findAll(){
        return ModelMapper.mapFromCollection(AdminTransfer.class, adminRepository.findAll());
    }

    @Transactional
    public void update(AdminTransfer transfer){
        Admin admin = adminRepository.findById(transfer.getId())
                .orElseThrow(()-> RepositoryException.ITEM_NOT_FOUND);
        ModelMapper.map(admin, transfer);

        Address address = addressRepository.findById(admin.getAddressId())
                .orElseThrow(()-> RepositoryException.ITEM_NOT_FOUND);
        ModelMapper.map(address, transfer.getAddress());
        addressRepository.save(admin.getAddress());
        adminRepository.save(admin);
    }


    @Transactional
    public void create(AdminCreateTransfer transfer){
        Address address = ModelMapper.mapFrom(Address.class, transfer.getAddress());
        address = addressRepository.save(address);
        Admin admin = ModelMapper.mapFrom(Admin.class, transfer);
        admin.setAddressId(address.getId());
        adminRepository.save(admin);
    }

    @Transactional
    public void delete(long id){
        Admin admin = adminRepository.findById(id)
                .orElseThrow(()-> RepositoryException.ITEM_NOT_FOUND);
        adminRepository.deleteById(id);
        adminRepository.flush();
        addressRepository.deleteById(admin.getAddressId());
    }

}
