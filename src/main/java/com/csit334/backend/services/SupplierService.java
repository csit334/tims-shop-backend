package com.csit334.backend.services;

import com.csit334.backend.entity.Address;
import com.csit334.backend.entity.Supplier;
import com.csit334.backend.repositories.AddressRepository;
import com.csit334.backend.repositories.SupplierRepository;
import com.csit334.backend.transfers.AddressTransfer;
import com.csit334.backend.transfers.SupplierCreateTransfer;
import com.csit334.backend.transfers.SupplierTransfer;
import org.dynabiz.mapper.ModelMapper;
import org.dynabiz.std.exception.RepositoryException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SupplierService {
    @Autowired
    private SupplierRepository supplierRepository;
    @Autowired
    private AddressRepository addressRepository;

    public SupplierTransfer findOne(long id){

         Supplier supplier = supplierRepository.findById(id)
                 .orElseThrow(()->RepositoryException.ITEM_NOT_FOUND);
         Address address = addressRepository.findById(supplier.getAddressId())
                 .orElseThrow(()-> RepositoryException.ITEM_NOT_FOUND);
         SupplierTransfer transfer = ModelMapper.mapFrom(SupplierTransfer.class, supplier);
         transfer.setAddress(ModelMapper.mapFrom(AddressTransfer.class, address));
         return transfer;
    }

    public List<SupplierTransfer> findAll(){
        return ModelMapper.mapFromCollection(SupplierTransfer.class, supplierRepository.findAll(),
                (s, t)->t.setAddress(ModelMapper.mapFrom(AddressTransfer.class, s.getAddressByAddressId())));
    }

    @Transactional
    public void update(SupplierTransfer transfer){
        Supplier supplier = supplierRepository.findById(transfer.getId())
                .orElseThrow(()-> RepositoryException.ITEM_NOT_FOUND);
        ModelMapper.map(supplier, transfer);

        Address address = addressRepository.findById(supplier.getAddressId())
                .orElseThrow(()->RepositoryException.ITEM_NOT_FOUND);
        ModelMapper.map(address, transfer.getAddress());

        addressRepository.save(address);
        supplierRepository.save(supplier);
    }

    @Transactional
    public void create(SupplierCreateTransfer transfer){
        Address address = ModelMapper.mapFrom(Address.class, transfer.getAddress());
        address = addressRepository.saveAndFlush(address);
        Supplier supplier = ModelMapper.mapFrom(Supplier.class, transfer);
        supplier.setAddressId(address.getId());
        supplierRepository.save(supplier);
    }

    @Transactional
    public void delete(long id){
        Supplier supplier = supplierRepository.findById(id)
                .orElseThrow(()-> RepositoryException.ITEM_NOT_FOUND);
        supplierRepository.deleteById(id);
        addressRepository.deleteById(supplier.getAddressId());

    }


}
