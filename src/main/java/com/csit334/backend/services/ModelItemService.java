package com.csit334.backend.services;


import com.csit334.backend.entity.ModelItem;
import com.csit334.backend.repositories.ModelItemRepository;
import com.csit334.backend.transfers.ModelItemCreateTransfer;
import com.csit334.backend.transfers.ModelItemTransfer;
import org.dynabiz.mapper.ModelMapper;
import org.dynabiz.std.exception.RepositoryException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ModelItemService {
    @Autowired
    private ModelItemRepository modelItemRepository;


    public ModelItemTransfer findOne(long id){

        return ModelMapper.mapFrom(ModelItemTransfer.class,
                modelItemRepository.findById(id).orElseThrow(()->RepositoryException.ITEM_NOT_FOUND));
    }

    public List<ModelItemTransfer> findAll(){
        return ModelMapper.mapFromCollection(ModelItemTransfer.class, modelItemRepository.findAll());
    }

    @Transactional
    public void create(ModelItemCreateTransfer transfer){
        modelItemRepository.saveAndFlush(ModelMapper.mapFrom(ModelItem.class, transfer));
    }

    @Transactional
    public void update(ModelItemTransfer transfer){
        modelItemRepository.saveAndFlush(ModelMapper.mapFrom(ModelItem.class, transfer));
    }

    @Transactional
    public void delete(long id){
        modelItemRepository.deleteById(id);
    }

}
