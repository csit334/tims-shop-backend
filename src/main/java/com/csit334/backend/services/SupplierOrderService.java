package com.csit334.backend.services;

import com.csit334.backend.entity.ModelItem;
import com.csit334.backend.entity.OrderItem;
import com.csit334.backend.entity.SupplierOrder;
import com.csit334.backend.entity.SupplierOrderItem;
import com.csit334.backend.repositories.*;
import com.csit334.backend.transfers.*;
import org.dynabiz.mapper.ModelMapper;
import org.dynabiz.std.exception.RepositoryException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SupplierOrderService {
    @Autowired
    private SupplierOrderItemRepository orderItemRepository;
    @Autowired
    private SupplierOrderRepository orderRepository;
    @Autowired
    private ModelItemRepository modelItemRepository;


    @Transactional
    public SupplierOrderTransfer findOne(long id){
        SupplierOrder order = orderRepository.findById(id)
                .orElseThrow(()-> RepositoryException.ITEM_NOT_FOUND);

        SupplierOrderTransfer transfer = ModelMapper.mapFrom(SupplierOrderTransfer.class, order);
        transfer.setItems(ModelMapper.mapFromCollection(SupplierOrderItemTransfer.class, order.getSupplierOrderItemsById()));
        return transfer;
    }

    public List<SupplierOrderTransfer> findAll(){
        return ModelMapper.mapFromCollection(SupplierOrderTransfer.class, orderRepository.findAll());
    }

    @Transactional
    public void update(SupplierOrderTransfer transfer){
        SupplierOrder order = orderRepository.findById(transfer.getId())
                .orElseThrow(()-> RepositoryException.ITEM_NOT_FOUND);
        ModelMapper.map(order, transfer);

        orderRepository.save(order);
    }

    @Transactional
    public void create(SupplierOrderCreateTransfer transfer){
        SupplierOrder order = orderRepository.save(ModelMapper.mapFrom(SupplierOrder.class, transfer));
        List<SupplierOrderItem> supplierOrderItems = ModelMapper.mapFromCollection(SupplierOrderItem.class, transfer.getItems(), (s, t)->{
            t.setSupplierOrderId(order.getId());
        });
        orderItemRepository.saveAll(supplierOrderItems);
        orderRepository.save(order);

        List<ModelItem> modelItems = modelItemRepository.findAllById(supplierOrderItems.stream().map(SupplierOrderItem::getModelItemId).collect(Collectors.toList()));
        Iterator<ModelItem> modelItemIterator = modelItems.iterator();
        for(SupplierOrderItem i : supplierOrderItems){
            ModelItem modelItem = modelItemIterator.next();
            modelItem.setStock(modelItem.getStock() + i.getQuantity());
        }
        modelItemRepository.saveAll(modelItems);
    }

    @Transactional
    public void delete(long id){
        SupplierOrder order = orderRepository.findById(id)
                .orElseThrow(()-> RepositoryException.ITEM_NOT_FOUND);
        orderItemRepository.deleteAllBySupplierOrderId(order.getId());
        orderRepository.deleteById(id);
    }


}
