package com.csit334.backend;

import org.dynabiz.web.error.config.EnableGlobalErrorHandler;
import org.dynabiz.web.response.config.EnableGeneralResponse;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;


@EnableGlobalErrorHandler
@EnableGeneralResponse
@SpringBootApplication
public class BackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(BackendApplication.class, args);
    }

}
