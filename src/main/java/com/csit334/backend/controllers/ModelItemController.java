package com.csit334.backend.controllers;


import com.csit334.backend.services.ModelItemService;
import com.csit334.backend.transfers.ModelItemCreateTransfer;
import com.csit334.backend.transfers.ModelItemTransfer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RequestMapping("model-items")
@RestController
public class ModelItemController {
    @Autowired
    private ModelItemService modelItemService;

    @GetMapping
    public List<ModelItemTransfer> findAll(){
        return modelItemService.findAll();
    }

    @GetMapping("/{id}")
    public ModelItemTransfer findOne(@PathVariable long id){
        return modelItemService.findOne(id);
    }

    @PostMapping
    public void create(@RequestBody ModelItemCreateTransfer transfer){
        modelItemService.create(transfer);
    }

    @PutMapping
    public void update(@RequestBody ModelItemTransfer transfer){
        modelItemService.update(transfer);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id){
        modelItemService.delete(id);
    }

}
