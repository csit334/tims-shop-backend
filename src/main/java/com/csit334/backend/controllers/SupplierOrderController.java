package com.csit334.backend.controllers;

import com.csit334.backend.entity.SupplierOrder;
import com.csit334.backend.entity.SupplierOrderItem;
import com.csit334.backend.repositories.SupplierOrderItemRepository;
import com.csit334.backend.repositories.SupplierOrderRepository;
import com.csit334.backend.services.SupplierOrderService;
import com.csit334.backend.services.SupplierService;
import com.csit334.backend.transfers.AdminTransfer;
import com.csit334.backend.transfers.OrderTransfer;
import com.csit334.backend.transfers.SupplierOrderCreateTransfer;
import com.csit334.backend.transfers.SupplierOrderTransfer;
import org.dynabiz.mapper.ModelMapper;
import org.dynabiz.std.exception.RepositoryException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("supplier-orders")
public class SupplierOrderController {
    @Autowired
    private SupplierOrderService supplierOrderService;

    @GetMapping("/{id}")
    public SupplierOrderTransfer findOne(@PathVariable long id){
        return supplierOrderService.findOne(id);
    }

    @GetMapping
    public List<SupplierOrderTransfer> findAll(){
        return supplierOrderService.findAll();
    }

    @PutMapping
    public void update(@RequestBody SupplierOrderTransfer transfer){
        supplierOrderService.update(transfer);
    }

    @PostMapping
    public void create(@RequestBody SupplierOrderCreateTransfer transfer){
        supplierOrderService.create(transfer);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id){
        supplierOrderService.delete(id);
    }


}
