package com.csit334.backend.controllers;

import com.csit334.backend.services.OrderService;
import com.csit334.backend.transfers.OrderCreateTransfer;
import com.csit334.backend.transfers.OrderTransfer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RequestMapping("orders")
@RestController
public class OrderController {
    @Autowired
    private OrderService orderService;

    @GetMapping("/{id}")
    public OrderTransfer findOne(@PathVariable long id){
        return orderService.findOne(id);
    }

    @GetMapping
    public List<OrderTransfer> findAll(){
        return orderService.findAll();
    }


    @PutMapping
    public void update(@RequestBody OrderTransfer transfer){
        orderService.update(transfer);
    }

    @PostMapping
    public void create(@RequestBody OrderCreateTransfer transfer){
        orderService.create(transfer);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id){
        orderService.delete(id);
    }


}
