package com.csit334.backend.controllers;


import com.csit334.backend.entity.Address;
import com.csit334.backend.entity.Admin;
import com.csit334.backend.repositories.AddressRepository;
import com.csit334.backend.repositories.AdminRepository;
import com.csit334.backend.services.AdminService;
import com.csit334.backend.transfers.*;
import org.dynabiz.mapper.ModelMapper;
import org.dynabiz.std.exception.RepositoryException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("admins")
@RestController
public class AdminController {
    @Autowired
    private AdminService adminService;


    @GetMapping
    public List<AdminTransfer> findAll(){
        return adminService.findAll();
    }

    @GetMapping("{id}")
    public AdminTransfer findOne(@PathVariable long id){
        return adminService.findOne(id);
    }


    @PutMapping
    public void update(@RequestBody AdminTransfer transfer){
        adminService.update(transfer);
    }

    public void changePassword(ChangePasswordTransfer transfer){

//        adminRepository.findById()
//                .orElseThrow(()-> RepositoryException.ITEM_NOT_FOUND);

    }

    @PostMapping
    public void create(@RequestBody AdminCreateTransfer transfer){
        adminService.create(transfer);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id){
        adminService.delete(id);
    }

}
