package com.csit334.backend.controllers;


import com.csit334.backend.services.CustomerService;
import com.csit334.backend.transfers.CustomerCreateTransfer;
import com.csit334.backend.transfers.CustomerTransfer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("customers")
public class CustomerController {
    @Autowired
    private CustomerService customerService;


    @GetMapping
    public List<CustomerTransfer> findAll(){
        return customerService.findAll();
    }

    @GetMapping("/{id}")
    public CustomerTransfer findOne(@PathVariable long id){
        return customerService.findOne(id);
    }


    @PostMapping
    public void create(@RequestBody CustomerCreateTransfer transfer){
        customerService.create(transfer);
    }





}
