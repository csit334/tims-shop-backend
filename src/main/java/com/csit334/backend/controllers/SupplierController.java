package com.csit334.backend.controllers;

import com.csit334.backend.services.SupplierService;
import com.csit334.backend.transfers.SupplierCreateTransfer;
import com.csit334.backend.transfers.SupplierTransfer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@CrossOrigin
@RestController
@RequestMapping("suppliers")
public class SupplierController {
    @Autowired
    private SupplierService supplierService;



    @GetMapping(value = "/{id}")
    public SupplierTransfer findOne(@PathVariable long id){
        return supplierService.findOne(id);
    }

    @GetMapping
    public List<SupplierTransfer> findAll(){
        return supplierService.findAll();
    }

    @PutMapping
    public void update(@RequestBody SupplierTransfer transfer){
        supplierService.update(transfer);
    }

    @PostMapping
    public void create(@RequestBody SupplierCreateTransfer transfer){
        supplierService.create(transfer);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id){
        supplierService.delete(id);
    }


}
