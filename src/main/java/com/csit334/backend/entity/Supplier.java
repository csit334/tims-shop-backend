package com.csit334.backend.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Objects;

@Entity
public class Supplier {
    private long id;
    private String name;
    private long addressId;
    private String deliveryDetails;
    private String contactPersons;
    private BigDecimal creditLine;
    private Address addressByAddressId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 30)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "address_id", nullable = false)
    public long getAddressId() {
        return addressId;
    }

    public void setAddressId(long addressId) {
        this.addressId = addressId;
    }

    @Basic
    @Column(name = "delivery_details", nullable = false, length = -1)
    public String getDeliveryDetails() {
        return deliveryDetails;
    }

    public void setDeliveryDetails(String deliveryDetails) {
        this.deliveryDetails = deliveryDetails;
    }

    @Basic
    @Column(name = "contact_persons", nullable = false, length = 30)
    public String getContactPersons() {
        return contactPersons;
    }

    public void setContactPersons(String contactPersons) {
        this.contactPersons = contactPersons;
    }

    @Basic
    @Column(name = "credit_line", nullable = true, precision = 2)
    public BigDecimal getCreditLine() {
        return creditLine;
    }

    public void setCreditLine(BigDecimal creditLine) {
        this.creditLine = creditLine;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Supplier supplier = (Supplier) o;
        return id == supplier.id &&
                addressId == supplier.addressId &&
                Objects.equals(name, supplier.name) &&
                Objects.equals(deliveryDetails, supplier.deliveryDetails) &&
                Objects.equals(contactPersons, supplier.contactPersons) &&
                Objects.equals(creditLine, supplier.creditLine);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, addressId, deliveryDetails, contactPersons, creditLine);
    }

    @OneToOne(targetEntity = Address.class)
    @JoinColumn(name = "address_id", referencedColumnName = "id", insertable=false, updatable=false)
    public Address getAddressByAddressId() {
        return addressByAddressId;
    }

    public void setAddressByAddressId(Address addressByAddressId) {
        this.addressByAddressId = addressByAddressId;
    }

}
