package com.csit334.backend.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "model_item", schema = "CSCI334", catalog = "")
public class ModelItem {
    private long id;
    private String name;
    private String modelType;
    private String subjectArea;
    private int stock;
    private BigDecimal price;
    private Timestamp instockDate;
    private String location;
    private String description;
    private Byte available;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 30)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "model_type", nullable = false, length = 64)
    public String getModelType() {
        return modelType;
    }

    public void setModelType(String modelType) {
        this.modelType = modelType;
    }

    @Basic
    @Column(name = "subject_area", nullable = false, length = 64)
    public String getSubjectArea() {
        return subjectArea;
    }

    public void setSubjectArea(String subjectArea) {
        this.subjectArea = subjectArea;
    }

    @Basic
    @Column(name = "stock", nullable = false)
    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    @Basic
    @Column(name = "price", nullable = false, precision = 2)
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Basic
    @Column(name = "instock_date", nullable = false)
    public Timestamp getInstockDate() {
        return instockDate;
    }

    public void setInstockDate(Timestamp instockDate) {
        this.instockDate = instockDate;
    }

    @Basic
    @Column(name = "location", nullable = false, length = 10)
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Basic
    @Column(name = "description", nullable = true, length = -1)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "available", nullable = true)
    public Byte getAvailable() {
        return available;
    }

    public void setAvailable(Byte available) {
        this.available = available;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ModelItem modelItem = (ModelItem) o;
        return id == modelItem.id &&
                stock == modelItem.stock &&
                Objects.equals(name, modelItem.name) &&
                Objects.equals(modelType, modelItem.modelType) &&
                Objects.equals(subjectArea, modelItem.subjectArea) &&
                Objects.equals(price, modelItem.price) &&
                Objects.equals(instockDate, modelItem.instockDate) &&
                Objects.equals(location, modelItem.location) &&
                Objects.equals(description, modelItem.description) &&
                Objects.equals(available, modelItem.available);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, modelType, subjectArea, stock, price, instockDate, location, description, available);
    }


}
