package com.csit334.backend.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "supplier_order", schema = "CSCI334", catalog = "")
public class SupplierOrder {
    private long id;
    private Timestamp datetime;
    private Long adminId;
    private BigDecimal price;
    private Admin adminByAdminId;
    private Long supplierId;
    private Collection<SupplierOrderItem> supplierOrderItemsById;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "datetime", nullable = false)
    public Timestamp getDatetime() {
        return datetime;
    }

    public void setDatetime(Timestamp datetime) {
        this.datetime = datetime;
    }

    @Basic
    @Column(name = "admin_id", nullable = true)
    public Long getAdminId() {
        return adminId;
    }

    public void setAdminId(Long adminId) {
        this.adminId = adminId;
    }

    @Basic
    @Column(name = "supplier_id", nullable = true)
    public Long getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Long supplierId) {
        this.supplierId = supplierId;
    }

    @Basic
    @Column(name = "price", nullable = false, precision = 2)
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal total) {
        this.price = total;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SupplierOrder that = (SupplierOrder) o;
        return id == that.id &&
                Objects.equals(datetime, that.datetime) &&
                Objects.equals(adminId, that.adminId) &&
                Objects.equals(price, that.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, datetime, adminId, price);
    }


    @ManyToOne
    @JoinColumn(name = "admin_id", referencedColumnName = "id", insertable=false, updatable=false)
    public Admin getAdminByAdminId() {
        return adminByAdminId;
    }

    public void setAdminByAdminId(Admin adminByAdminId) {
        this.adminByAdminId = adminByAdminId;
    }

    @JoinColumn(name = "supplier_order_id", updatable = false)
    @OneToMany(targetEntity = SupplierOrderItem.class, cascade = CascadeType.REMOVE, orphanRemoval = true)
    public Collection<SupplierOrderItem> getSupplierOrderItemsById() {
        return supplierOrderItemsById;
    }

    public void setSupplierOrderItemsById(Collection<SupplierOrderItem> supplierOrderItemsById) {
        this.supplierOrderItemsById = supplierOrderItemsById;
    }
}
