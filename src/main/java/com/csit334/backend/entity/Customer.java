package com.csit334.backend.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
public class Customer {
    private long id;
    private String firstName;
    private String lastName;
    private long addressId;
    private String phoneNumber;
    private BigDecimal creditLine;
    private BigDecimal balance;
    private byte member;
    private Timestamp joinAt;
    private String email;
    private String username;
    private String passwordSha256;
    private Address address;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "first_name", nullable = false, length = 10)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Basic
    @Column(name = "last_name", nullable = false, length = 10)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Basic
    @Column(name = "address_id", nullable = false)
    public long getAddressId() {
        return addressId;
    }

    public void setAddressId(long addressId) {
        this.addressId = addressId;
    }

    @Basic
    @Column(name = "phone_number", nullable = false, length = 15)
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Basic
    @Column(name = "credit_line", nullable = false, precision = 2)
    public BigDecimal getCreditLine() {
        return creditLine;
    }

    public void setCreditLine(BigDecimal creditLine) {
        this.creditLine = creditLine;
    }

    @Basic
    @Column(name = "balance", nullable = false, precision = 2)
    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    @Basic
    @Column(name = "member", nullable = false)
    public byte getMember() {
        return member;
    }

    public void setMember(byte member) {
        this.member = member;
    }

    @Basic
    @Column(name = "join_at", nullable = false)
    public Timestamp getJoinAt() {
        return joinAt;
    }

    public void setJoinAt(Timestamp joinAt) {
        this.joinAt = joinAt;
    }

    @Basic
    @Column(name = "email", nullable = true, length = 128)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "username", nullable = false, length = 20)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "password_sha256", nullable = true, length = 60)
    public String getPasswordSha256() {
        return passwordSha256;
    }

    public void setPasswordSha256(String passwordSha256) {
        this.passwordSha256 = passwordSha256;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return id == customer.id &&
                addressId == customer.addressId &&
                member == customer.member &&
                Objects.equals(firstName, customer.firstName) &&
                Objects.equals(lastName, customer.lastName) &&
                Objects.equals(phoneNumber, customer.phoneNumber) &&
                Objects.equals(creditLine, customer.creditLine) &&
                Objects.equals(balance, customer.balance) &&
                Objects.equals(joinAt, customer.joinAt) &&
                Objects.equals(email, customer.email) &&
                Objects.equals(username, customer.username) &&
                Objects.equals(passwordSha256, customer.passwordSha256);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, addressId, phoneNumber, creditLine, balance, member, joinAt, email, username, passwordSha256);
    }

    @OneToOne(targetEntity = Address.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id", referencedColumnName = "id", insertable=false, updatable=false)
    public Address getAddress() {
        return address;
    }

    public void setAddress(Address addressByAddressId) {
        this.address = addressByAddressId;
    }

}
