package com.csit334.backend.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "supplier_order_item", schema = "CSCI334", catalog = "")
public class SupplierOrderItem {
    private long id;
    private int quantity;
    private BigDecimal unitPrice;
    private long supplierOrderId;
    private long modelItemId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    @Basic
    @Column(name = "quantity", nullable = false)
    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Basic
    @Column(name = "unit_price", nullable = false, precision = 2)
    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SupplierOrderItem that = (SupplierOrderItem) o;
        return id == that.id &&
                quantity == that.quantity &&
                supplierOrderId == that.supplierOrderId &&
                modelItemId == that.modelItemId &&
                Objects.equals(unitPrice, that.unitPrice);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, quantity, unitPrice, supplierOrderId, modelItemId);
    }

    @Basic
    @Column(name = "supplier_order_id", nullable = false)
    public long getSupplierOrderId() {
        return supplierOrderId;
    }

    public void setSupplierOrderId(long supplierOrderId) {
        this.supplierOrderId = supplierOrderId;
    }

    @Basic
    @Column(name = "model_item_id", nullable = false)
    public long getModelItemId() {
        return modelItemId;
    }

    public void setModelItemId(long modelItemId) {
        this.modelItemId = modelItemId;
    }
}
