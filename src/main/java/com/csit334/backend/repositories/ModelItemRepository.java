package com.csit334.backend.repositories;

import com.csit334.backend.entity.ModelItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ModelItemRepository extends JpaRepository<ModelItem, Long> {
}
