package com.csit334.backend.repositories;

import com.csit334.backend.entity.OrderItem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderItemRepository extends JpaRepository<OrderItem, Long> {
    List<OrderItem> findAllByOrderId(long id);
    List<OrderItem> deleteDistinctByOrderId(long id);
}
