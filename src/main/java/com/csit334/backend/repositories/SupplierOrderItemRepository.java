package com.csit334.backend.repositories;

import com.csit334.backend.entity.SupplierOrderItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SupplierOrderItemRepository extends JpaRepository<SupplierOrderItem, Long> {
    void deleteAllBySupplierOrderId(long id);
}
