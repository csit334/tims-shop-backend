package com.csit334.backend.transfers;

import lombok.Data;
import org.dynabiz.mapper.MappingField;

import java.util.List;

@Data
public class OrderItemTransfer extends OrderItemCreateTransfer {
    @MappingField
    private long id;
}
