package com.csit334.backend.transfers;

import lombok.Data;
import org.dynabiz.mapper.MappingField;

import java.math.BigDecimal;

@Data
public class OrderItemCreateTransfer {
    @MappingField
    private long modelItemId;
    @MappingField
    private BigDecimal price;
    @MappingField
    private int quantity;
    
}
