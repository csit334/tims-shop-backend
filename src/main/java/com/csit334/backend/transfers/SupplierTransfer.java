package com.csit334.backend.transfers;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.dynabiz.mapper.MappingField;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class SupplierTransfer extends SupplierCreateTransfer{
    @MappingField
    private long id;
}
