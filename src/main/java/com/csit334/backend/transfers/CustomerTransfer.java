package com.csit334.backend.transfers;

import lombok.Data;
import org.dynabiz.mapper.MappingField;

import java.math.BigDecimal;
import java.sql.Timestamp;

@Data
public class CustomerTransfer extends CustomerCreateTransfer {
    @MappingField
    private long id;
}
