package com.csit334.backend.transfers;

import com.csit334.backend.entity.SupplierOrderItem;
import lombok.Data;
import org.dynabiz.mapper.MappingField;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

@Data
public class SupplierOrderTransfer {
    @MappingField
    private long id;
    @MappingField
    private Timestamp datetime;
    @MappingField
    private Long adminId;
    @MappingField
    private BigDecimal price;

    @MappingField
    private Long supplierId;

    private List<SupplierOrderItemTransfer> items;
}
