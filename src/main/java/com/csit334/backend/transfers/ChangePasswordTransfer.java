package com.csit334.backend.transfers;

import lombok.Data;
import org.dynabiz.mapper.MappingField;

@Data
public class ChangePasswordTransfer {
    private String oldPassword;
    private String newPassword;
}
