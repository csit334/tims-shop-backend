package com.csit334.backend.transfers;

import lombok.Data;
import org.dynabiz.mapper.MappingField;

@Data
public class AdminTransfer {
    @MappingField
    private long id;
    @MappingField
    private String firstName;
    @MappingField
    private String lastName;
    @MappingField
    private String phoneNumber;
    @MappingField
    private String username;

    @MappingField
    private String password;

    private AddressTransfer address;
}
