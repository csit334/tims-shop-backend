package com.csit334.backend.transfers;

import lombok.Data;
import org.dynabiz.mapper.MappingField;

import java.math.BigDecimal;

@Data
public class SupplierCreateTransfer {
    private AddressTransfer address;

    @MappingField
    private String name;
    @MappingField
    private String deliveryDetails;
    @MappingField
    private String contactPersons;
    @MappingField
    private BigDecimal creditLine;


}
