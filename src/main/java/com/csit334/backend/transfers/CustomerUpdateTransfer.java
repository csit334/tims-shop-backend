package com.csit334.backend.transfers;

import lombok.Data;
import org.dynabiz.mapper.MappingField;

import java.math.BigDecimal;
import java.sql.Timestamp;

@Data
public class CustomerUpdateTransfer {
    @MappingField
    private long id;
    @MappingField
    private String firstName;
    @MappingField
    private String lastName;

    AddressTransfer addressTransfer;

    @MappingField
    private String phoneNumber;
    @MappingField
    private BigDecimal creditLine;
    @MappingField
    private BigDecimal balance;
    @MappingField
    private Boolean member;
    private Timestamp joinAt;
    private String email;
    private String username;
}
