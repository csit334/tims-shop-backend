package com.csit334.backend.transfers;

import lombok.Data;
import org.dynabiz.mapper.MappingField;


@Data
public class ModelItemTransfer extends ModelItemCreateTransfer{
    @MappingField
    private long id;


}
