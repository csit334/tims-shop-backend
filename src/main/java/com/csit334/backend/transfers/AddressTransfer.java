package com.csit334.backend.transfers;


import lombok.Data;
import lombok.NoArgsConstructor;
import org.dynabiz.mapper.MappingField;

@Data
@NoArgsConstructor
public class AddressTransfer {
    @MappingField
    private String unitNumber;
    @MappingField
    private String streetNumber;
    @MappingField
    private String streetName;
    @MappingField
    private String suburb;
    @MappingField
    private String state;
    @MappingField
    private String postcode;
    @MappingField
    private String country;
}
