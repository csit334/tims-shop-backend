package com.csit334.backend.transfers;

import lombok.Data;
import org.dynabiz.mapper.MappingField;

import java.math.BigDecimal;
import java.sql.Timestamp;

@Data
public class CustomerCreateTransfer {

    @MappingField
    private String firstName;
    @MappingField
    private String lastName;
    @MappingField
    private long addressId;
    @MappingField
    private String phoneNumber;
    @MappingField
    private BigDecimal creditLine;
    @MappingField
    private BigDecimal balance;
    @MappingField
    private byte member;
    @MappingField
    private Timestamp joinAt;
    @MappingField
    private String email;
    @MappingField
    private String username;
    @MappingField
    private String passwordSha256;


    private AddressTransfer address;
}
