package com.csit334.backend.transfers;

import lombok.Data;
import org.dynabiz.mapper.MappingField;

import java.math.BigDecimal;
import java.sql.Timestamp;

@Data
public class ModelItemCreateTransfer {
    @MappingField
    private String name;
    @MappingField
    private String modelType;
    @MappingField
    private String subjectArea;
    @MappingField
    private int stock;
    @MappingField
    private BigDecimal price;
    @MappingField
    private Timestamp instockDate;
    @MappingField
    private String location;
    @MappingField
    private String description;
    @MappingField
    private Byte available;
}
