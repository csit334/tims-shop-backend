package com.csit334.backend.transfers;


import lombok.Data;
import lombok.NoArgsConstructor;
import org.dynabiz.mapper.MappingField;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

@Data
@NoArgsConstructor
public class OrderCreateTransfer {
    @MappingField
    private BigDecimal price;
    @MappingField
    private BigDecimal discount;
    @MappingField
    private long adminId;
    @MappingField
    private String state;

    @MappingField
    private Long customerId;
    @MappingField
    private Timestamp datetime;


    private List<OrderItemCreateTransfer> items;
}
