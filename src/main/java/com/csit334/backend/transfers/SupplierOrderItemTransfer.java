package com.csit334.backend.transfers;

import com.csit334.backend.entity.SupplierOrderItem;
import lombok.Data;
import org.dynabiz.mapper.MappingField;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

@Data
public class SupplierOrderItemTransfer extends SupplierOrderItemCreateTransfer{
    @MappingField
    private long id;
}
