package com.csit334.backend.transfers;

import lombok.Data;
import org.dynabiz.mapper.MappingField;

import java.math.BigDecimal;

@Data
public class SupplierOrderItemCreateTransfer {


    @MappingField
    private int quantity;
    @MappingField
    private BigDecimal unitPrice;
    @MappingField
    private long modelItemId;
}
